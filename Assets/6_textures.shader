﻿Shader "ShaderTestLab/6-Textures" {
	Properties{
		_Color ("Color Tint", Color) = (1.0,1.0,1.0,1.0)
		_MainTex ("Diffuse Texture", 2D) = "white"{}
		_SpecColor ("Specular Color", Color) = (1.0,1.0,1.0,1.0)
		_Shininess ("Shininess", Float) = 10
		_RimColor ("RimColor", Color) = (1.0,1.0,1.0,1.0)
		_RimPower ("RimPower", Range(0.1,10.0)) = 3.0
	}
	SubShader{
		Pass{
			Tags{"LightMode" = "ForwardBase"} 
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers flash
			
			//user defined variables
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float4 _RimColor;
			uniform float4 _MainTex_ST;
			uniform float _Shininess;
			uniform float _RimPower;
			uniform sampler2D _MainTex;
			
			
			//Unity defined variables
			uniform float4 _LightColor0; 
	
			//base input structs
			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalDir : TEXCOORD2;
				
			};
			
			//vertex function
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.posWorld = mul(_Object2World, v.vertex);
				o.normalDir = normalize(mul(float4(v.normal, 0.0), _World2Object).xyz);
				o.tex = v.texcoord;
				
				return o;
			}
			
			//fragment funtion 
			float4 frag(vertexOutput i) : COLOR{

				//Vectors
				float3 normalDirection = i.normalDir;
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
				float3 lightDirection;
				float attenuation;
				
												
				if(_WorldSpaceLightPos0.w == 0.0){//directional Light
					attenuation = 1.0;
					lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				}else{//PointLights
					float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
					float distance = length(fragmentToLightSource);
					attenuation = 1.0/distance;
					lightDirection = normalize(fragmentToLightSource);
				}	
				
				//lighting
				float3 diffuseReflection = attenuation * _LightColor0.xyz * saturate(dot(normalDirection, lightDirection));
				float3 specularReflection = diffuseReflection * _SpecColor.xyz * pow(saturate(dot(reflect( -lightDirection, normalDirection), viewDirection)), _Shininess);
				
				//Rim lighting
				float rim = 1 - saturate(dot(viewDirection, normalDirection));// Mask
				float3 rimLighting = saturate(dot(normalDirection, lightDirection) * _LightColor0.xyz * _RimColor.xyz * pow(rim, _RimPower));
				
				float3 lightFinal = rimLighting + diffuseReflection + specularReflection + UNITY_LIGHTMODEL_AMBIENT.xyz;
				
				//Texture Maps
				float4 tex = tex2D(_MainTex, i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);// unwrap function
				
				return float4(tex.xyz * lightFinal * _Color.xyz, 1.0);
			}
			
			ENDCG
		}
		
		Pass{
			Tags{"LightMode" = "ForwardAdd"} 
			Blend One One
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma exclude_renderers flash
			
			//user defined variables
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float4 _RimColor;
			uniform float4 _MainTex_ST;
			uniform float _Shininess;
			uniform float _RimPower;
			uniform sampler2D _MainTex;
			
			
			//Unity defined variables
			uniform float4 _LightColor0;
	
			//base input structs
			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float3 normalDir : TEXCOORD2;
				
			};
			
			//vertex function
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.posWorld = mul(_Object2World, v.vertex);
				o.normalDir = normalize(mul(float4(v.normal, 0.0), _World2Object).xyz);
				o.tex = v.texcoord;
				
				return o;
			}
			
			//fragment funtion 
			float4 frag(vertexOutput i) : COLOR{

				//Vectors
				float3 normalDirection = i.normalDir;
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
				float3 lightDirection;
				float attenuation;
				
												
				if(_WorldSpaceLightPos0.w == 0.0){//directional Light
					attenuation = 1.0;
					lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				}else{//PointLights
					float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - i.posWorld.xyz;
					float distance = length(fragmentToLightSource);
					attenuation = 1.0/distance;
					lightDirection = normalize(fragmentToLightSource);
				}	
				
				//lighting
				float3 diffuseReflection = attenuation * _LightColor0.xyz * saturate(dot(normalDirection, lightDirection));
				float3 specularReflection = diffuseReflection * _SpecColor.xyz * pow(saturate(dot(reflect( -lightDirection, normalDirection), viewDirection)), _Shininess);
				
				//Rim lighting
				float rim = 1 - saturate(dot(viewDirection, normalDirection));// Mask
				float3 rimLighting = saturate(dot(normalDirection, lightDirection) * _LightColor0.xyz * _RimColor.xyz * pow(rim, _RimPower));
				
				float3 lightFinal = rimLighting + diffuseReflection + specularReflection + UNITY_LIGHTMODEL_AMBIENT.xyz;
				
				//Texture Maps
				float4 tex = tex2D(_MainTex, i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				
				return float4(tex.xyz * lightFinal * _Color.xyz, 1.0);
			}
			
			ENDCG
		}
	}
	//Fallback "Specular"
}
