﻿Shader "ShaderTestLab/7-NormalMaps" {
	Properties{
		_Color ("Color Tint", Color) = (1.0,1.0,1.0,1.0)
		_SpecColor ("Specular Color", Color) = (1.0,1.0,1.0,1.0)
		_Shininess ("Shininess", Float) = 10
		_RimColor ("RimColor", Color) = (1.0,1.0,1.0,1.0)
		_RimPower ("RimPower", Range(0.1,10.0)) = 3.0
		_MainTex ("Diffuse Texture", 2D) = "white"{}
		_NormalPower ("NormalPower", Range(-2.0, 2.0)) = 1.0
		_BumpMap ("Bumb Map", 2D) = "bumb"{}
	}
	SubShader{
		Pass{
			Tags{"LightMode" = "ForwardBase"}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			//user defined variables
			uniform fixed4 _Color;
			uniform fixed4 _SpecColor;
			uniform fixed4 _RimColor;
			
			uniform half _Shininess;
			uniform half _RimPower;
			uniform fixed _NormalPower;
			
			uniform sampler2D _MainTex;
			uniform half4 _MainTex_ST;
			uniform sampler2D _BumpMap;
			uniform half4 _BumpMap_ST;
			
			
			//Unity defined variables
			uniform half4 _LightColor0; 

			//base input structs
			struct vertexInput{
				half4 vertex : POSITION;
				half3 normal : NORMAL;
				half4 texcoord : TEXCOORD0;
				half4 tangent : TANGENT;
			};
			
			struct vertexOutput{
				half4 pos : SV_POSITION;
				half4 tex : TEXCOORD0;
				fixed4 lightDirection : TEXCOORD1;
				fixed3 viewDirection : TEXCOORD2;
				fixed3 normalWorld : TEXCOORD3;
				fixed3 tangentWorld : TEXCOORD4;
				fixed3 binormalWorld : TEXCOORD5;
				
			};
			
			//vertex function
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				o.normalWorld = normalize(mul(half4(v.normal.xyz, 0.0), _World2Object).xyz);
				o.tangentWorld = normalize(mul(_Object2World, v.tangent).xyz);
				o.binormalWorld = normalize(cross(o.normalWorld, o.tangentWorld) * v.tangent.w);
				
				half4 posWorld = mul(_Object2World, v.vertex);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.tex = v.texcoord;
				o.viewDirection = normalize(_WorldSpaceCameraPos.xyz - posWorld.xyz);
				
				half3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - posWorld.xyz;
				half distance = length(fragmentToLightSource);
				
				half attenuation = lerp(1.0, 1.0/distance, _WorldSpaceLightPos0.w);
				half3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w));
				o.lightDirection = half4 (lightDirection, attenuation);
				
				return o;
			}
			
			//fragment funtion 
			fixed4 frag(vertexOutput i) : COLOR{

				
				//Texture Maps
				fixed4 tex = tex2D(_MainTex, i.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);
				fixed4 texN = tex2D(_BumpMap, i.tex.xy * _BumpMap_ST.xy + _BumpMap_ST.zw);
				
				//unpackNormal function
				fixed3 localCoords = float3(2.0 * texN.ag - float2(1.0, 1.0),_NormalPower);
				
				//Normal transpose matrix
				half3x3 local2WorldTranspose = half3x3(
					i.tangentWorld,
					i.binormalWorld,
					i.normalWorld
				);
				
				//calculate normal direction
				fixed3 normalDirection = normalize( mul(localCoords, local2WorldTranspose));
				
				//lighting
				//dot product
				fixed nDotL = saturate(dot(normalDirection, i.lightDirection.xyz));
				
				fixed3 diffuseReflection = i.lightDirection.w * _LightColor0.xyz * nDotL;
				fixed3 specularReflection = diffuseReflection * _SpecColor.xyz * pow(saturate(dot(reflect( -i.lightDirection.xyz, normalDirection), i.viewDirection)), _Shininess);
				
				//Rim lighting
				fixed rim = 1 - nDotL;// Mask
				fixed3 rimLighting = nDotL * _LightColor0.xyz * _RimColor.xyz * pow(rim, _RimPower);
				
				fixed3 lightFinal = rimLighting + diffuseReflection + specularReflection + UNITY_LIGHTMODEL_AMBIENT.xyz;
				
				return fixed4(tex.xyz * lightFinal * _Color.xyz, 1.0);
			}
			
			ENDCG
		}
		Pass{
			Tags{"LightMode" = "ForwardAdd"} 
			blend One One
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			//user defined variables
			uniform fixed4 _Color;
			uniform fixed4 _SpecColor;
			uniform fixed4 _RimColor;
			
			uniform half _Shininess;
			uniform half _RimPower;
			uniform fixed _NormalPower;
			
			uniform sampler2D _MainTex;
			uniform half4 _MainTex_ST;
			uniform sampler2D _BumpMap;
			uniform half4 _BumpMap_ST;
			
			
			//Unity defined variables
			uniform half4 _LightColor0;
	
			//base input structs
			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
				float4 tangent : TANGENT;
			};
			
			struct vertexOutput{
				half4 pos : SV_POSITION;
				half4 tex : TEXCOORD0;
				fixed4 lightDirection : TEXCOORD1;
				fixed3 viewDirection : TEXCOORD2;
				fixed3 normalWorld : TEXCOORD3;
				fixed3 tangentWorld : TEXCOORD4;
				fixed3 binormalWorld : TEXCOORD5;
				
			};
			
			//vertex function
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				o.normalWorld = normalize(mul(half4(v.normal.xyz, 0.0), _World2Object).xyz);
				o.tangentWorld = normalize(mul(_Object2World, v.tangent).xyz);
				o.binormalWorld = normalize(cross(o.normalWorld, o.tangentWorld) * v.tangent.w);
				
				half4 posWorld = mul(_Object2World, v.vertex);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.tex = v.texcoord;
				o.viewDirection = normalize(_WorldSpaceCameraPos.xyz - posWorld.xyz);
				
				half3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - posWorld.xyz;
				half distance = length(fragmentToLightSource);
				
				half attenuation = lerp(1.0, 1.0/distance, _WorldSpaceLightPos0.w);
				half3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w));
				o.lightDirection = half4 (lightDirection, attenuation);
				
				return o;
			}
			
			//fragment funtion 
			fixed4 frag(vertexOutput i) : COLOR{

				
				//Texture Maps
				fixed4 texN = tex2D(_BumpMap, i.tex.xy * _BumpMap_ST.xy + _BumpMap_ST.zw);
				
				//unpackNormal function
				fixed3 localCoords = float3(2.0 * texN.ag - float2(1.0, 1.0),_NormalPower);
				
				//Normal transpose matrix
				half3x3 local2WorldTranspose = half3x3(
					i.tangentWorld,
					i.binormalWorld,
					i.normalWorld
				);
				
				//calculate normal direction
				fixed3 normalDirection = normalize( mul(localCoords, local2WorldTranspose));
				
				//lighting
				//dot product
				fixed nDotL = saturate(dot(normalDirection, i.lightDirection.xyz));
				
				fixed3 diffuseReflection = i.lightDirection.w * _LightColor0.xyz * nDotL;
				fixed3 specularReflection = diffuseReflection * _SpecColor.xyz * pow(saturate(dot(reflect( -i.lightDirection.xyz, normalDirection), i.viewDirection)), _Shininess);
				
				//Rim lighting
				fixed rim = 1 - nDotL;// Mask
				fixed3 rimLighting = nDotL * _LightColor0.xyz * _RimColor.xyz * pow(rim, _RimPower);
				
				fixed3 lightFinal = rimLighting + diffuseReflection + specularReflection;
				
				return fixed4(lightFinal, 1.0);
			}
			
			ENDCG
		}
	}
	//Fallback "Specular"
}
