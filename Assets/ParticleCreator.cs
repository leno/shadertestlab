﻿using UnityEngine;
using System.Collections;

public class ParticleCreator : MonoBehaviour {
	public Material particleMaterial;
	[Range(0.0f,0.5f)]
	public float velocityX = 0.0f;
	[Range(0.0f,0.5f)]
	public float velocityY = 0.0f;
	[Range(0.0f,0.5f)]
	public float velocityZ = 0.0f;
	// Use this for initialization
	void Start () {
		particleMaterial = (Material)Resources.Load("9_Particle", typeof(Material));
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit = new RaycastHit();
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			Debug.DrawRay(ray.origin,ray.direction * 50.0f, Color.green, 50.0f);
			if(Physics.Raycast(ray,out hit)){
				createParticle(hit);
			}
		}
	}

	void createParticle(RaycastHit hit){
		int particleCount = 1000;
		Vector3[] vertices = new Vector3[particleCount];
		Vector4[] shaderData = new Vector4[particleCount];
		Vector3[] normals = new Vector3[particleCount];
		int[] indeces = new int[particleCount];
		Debug.Log (hit.point.normalized);
		for(int i = 0; i < particleCount; ++i){
			vertices[i].x = Random.Range(0.0f,0.5f); // x ausdehnung
			vertices[i].y = Random.Range(0.0f,0.0f); // y ausdehnung
			vertices[i].z = Random.Range(-0.2f,0.0f); // z ausdehnung
			normals[i] = hit.point.normalized;
			indeces[i] = i;
			shaderData[i].x = Random.Range(0.0f,velocityX); // acceleration x
			shaderData[i].y = Random.Range(0.0f,velocityY);// acceleration y Random.Range(0.1f,0.5f);// velocity TODO kann dann auf die x Coordinate oder mit setFloat über part.renderer.material.setFloat muss auch noch in verschiedene Richtungen möglich sein
			shaderData[i].z = Random.Range(0.0f,velocityZ); // acceleration z
		}


		GameObject part = new GameObject("ParticleGO");
		part.AddComponent<MeshRenderer>();
		part.transform.position = hit.point;
		part.GetComponent<Renderer>().material = particleMaterial;
		MeshFilter mFilter = part.AddComponent<MeshFilter>();

		Mesh m = new Mesh();
		// einfach 2 vector3 vertecies arrays machen und diese immer swappen um dem shader
		m.vertices = vertices;
		m.normals = normals;
		m.name = "particle_Mesh";
		m.SetIndices(indeces, MeshTopology.Points, 0);
		m.tangents = shaderData;
		mFilter.mesh = m;
		Shader.SetGlobalFloat("_LifeTime", 4.0f);
		Shader.SetGlobalFloat("_velocityX", 1.0f);
		Shader.SetGlobalFloat("_velocityY", 1.0f);
		Shader.SetGlobalFloat("_velocityZ", 1.0f);
		Shader.SetGlobalFloat("_Size", 0.5f);
		
		//Mesh mesh = new Mesh();
		//GetComponent<MeshFilter>().mesh = mesh;
		//mesh.vertices = vertices;



		//MeshFilter meshFilter = particle.AddComponent<MeshFilter>();
		//meshFilter.mesh = mesh;

	}
}
