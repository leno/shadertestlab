﻿Shader "ShaderTestLab/1-Flat Color" {
	Properties {
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}
	
	SubShader {
		Pass {
			
			// alles ab hier wird in CG Compiliert, alles davor und danach in Shaderlab
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			
			//user defined variables
			uniform float4 _Color;
			
			// base input structs
			struct vertexInput{
				float4 vertex : POSITION; // applied die semantik von Position zum float4 (x,y,z,w)	
			};
			
			struct vertexOutput{// ist der output der Vertex function für die fragment function
				float4 pos : SV_POSITION; // applied die semantik von SV_POSITION zum float4 SV_POSITION verhält sich wie POSITION wird jedoch für directX 11 benötigt
			};
			
			//vertex function
			vertexOutput vert(vertexInput v){// holt die positionen unseres Objects von Unity
				vertexOutput o;
				// multipliziert die unity matrix mit den Vertecies um sie für Unity verwendbar zu machen 
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}
			
			//fragemten function 
			float4 frag(vertexOutput i) : COLOR{
				return _Color;
			}
			ENDCG
		}
	}
	//fallback if something went wrong with the obove code the fallback will be used instead
	//Fallback "Diffuse"
}