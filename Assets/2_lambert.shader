﻿Shader "ShaderTestLab/2-Lambert" {
	Properties{
		_Color ("Color", Color) = (1.0,1.0,1.0,1.0)
	}
	
	SubShader{
		Pass{
			Tags{"LightMode" = "ForwardBase"} // contain spectial information about the path(light rotation is recognized)  (Untiy specific)
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			//user defined variables
			uniform float4 _Color;
			
			//Unity defined variables
			uniform float4 _LightColor0; // Color of the light
			//Unity 3 definitions brauche ich hier nicht, habe unity 4 und da sind diese schon vordefiniert
			//float4x4 _Object2World;
			//float4x4 _World2Object;
			//float4 _WorldSpaceLightPos0;
			
			//base input structs
			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};
			
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};
			
			//vertex function
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				// Multiplizieren die normaldirection mit world2Object um in den Object space zu kommen
				// da world2Object ein float 4 ist und v.normal ein float3 müssen wir einen neuen float4 erstellen dazu nehmen wir v.normal und setzen w auf 0.0
				float3 normalDirection = normalize(mul(float4(v.normal, 0.0), _World2Object).xyz); 
				float3 lightDirection;
				float attenuation = 1.0; // abschwächung
				
				//normalisieren der worldSpaceLightPosition damit wir nacher korrekt damit rechnen können
				lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				
				//dot produkt berechnet werte von -1 bis 1 basiert auf der normal und der light  direction. Das max entfehrnt alle werte die kleiner sind.
				float3 diffuseReflection = attenuation * _LightColor0.xyz * _Color.rgb * max(0.0, dot(normalDirection, lightDirection));
				
				// 1.0 ist der Alpha wert den wir im float3 diffuseReflection nicht dabei hatten.
				o.col = float4(diffuseReflection, 1.0);
				
				// konvert the v.vertex into Unity space so that unity can understand it
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}
			
			//fragment funtion 
			float4 frag(vertexOutput i) : COLOR{
			
				return i.col;
			}
			
			
			
			ENDCG
		}
	}

}