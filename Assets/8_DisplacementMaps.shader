﻿Shader "ShaderTestLab/8-DisplacementMaps" {
	Properties{
		_Color ("Color Tint", Color) = (1.0,1.0,1.0,1.0)
		_SpecColor ("Specular Color", Color) = (1.0,1.0,1.0,1.0)
		_Shininess ("Shininess", Float) = 10
		_RimColor ("RimColor", Color) = (1.0,1.0,1.0,1.0)
		_RimPower ("RimPower", Range(0.1,10.0)) = 3.0
		_MainTex ("Diffuse Texture", 2D) = "white"{}
		_NormalPower ("NormalPower", Range(0.0, 2.0)) = 1.0
		_BumpMap ("Bumb Map", 2D) = "bumb"{}
		_Attenuation("Attenuation", Range(0.0, 1.0)) = 1.0
		_DisplacementMap ("Displacement Map", 2D) = "bumb"{}
		_DisplacementPower("Displacement Power", Range(0.0, 0.0015)) = 0.001
		_DisplacementRefinement("Displacement Refinement", Range(1, 10)) = 1
		_DisplacementStepCount("Displacement Step Count", Range(1, 10)) = 1
	}
	SubShader{
		Pass{
			Tags{"LightMode" = "ForwardBase"} // contain spectial information about the path(light rotation is recognized)  (Untiy specific)
			CGPROGRAM
			#pragma target 4.0
			#pragma vertex vert
			#pragma fragment frag
			 
			
			//user defined variables
			uniform fixed4 _Color;
			uniform fixed4 _SpecColor;
			uniform fixed4 _RimColor;
			
			uniform half _Shininess;
			uniform half _RimPower;
			uniform fixed _NormalPower;
			uniform float _Attenuation;
			
			uniform sampler2D _MainTex;
			uniform half4 _MainTex_ST;
			uniform sampler2D _BumpMap;
			uniform half4 _BumpMap_ST;
			uniform sampler2D _DisplacementMap;
			uniform half4 _Displacement_ST;
			uniform fixed _DisplacementPower;
			uniform int _DisplacementRefinement;
			uniform int _DisplacementStepCount;
			
			
			//Unity defined variables
			uniform half4 _LightColor0; // Color of the light
	
			//base input structs
			struct vertexInput{
				half4 vertex : POSITION;
				half3 normal : NORMAL;
				half4 texcoord : TEXCOORD0;
				half4 tangent : TANGENT;
			};
			
			struct vertexOutput{
				half4 pos : SV_POSITION;
				half4 tex : TEXCOORD0;
				fixed4 lightDirection : TEXCOORD1;
				fixed3 viewDirection : TEXCOORD2;
				fixed3 normalWorld : TEXCOORD3;
				fixed3 tangentWorld : TEXCOORD4;
				fixed3 binormalWorld : TEXCOORD5;
			};
			
			//vertex function
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				o.normalWorld = normalize(mul(half4(v.normal.xyz, 0.0), _World2Object).xyz);
				o.tangentWorld = normalize(mul(_Object2World, v.tangent).xyz);
				o.binormalWorld = normalize(cross(o.normalWorld, o.tangentWorld) * v.tangent.w);
				
				half4 posWorld = mul(_Object2World, v.vertex);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.tex = v.texcoord;
				o.viewDirection = normalize(_WorldSpaceCameraPos.xyz - posWorld.xyz);
				
				half3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - posWorld.xyz;
				half distance = length(fragmentToLightSource);
				
				half attenuation = _Attenuation;
				half3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w));
				o.lightDirection = half4 (lightDirection, attenuation);
				
				return o;
			}
			
			//fragment funtion 
			fixed4 frag(vertexOutput i) : COLOR{

				float2 NewCoords = i.tex;	
				float2 dUV = -i.viewDirection.xy * _DisplacementPower;
				float prev_hits = 0.0;
				float h = 1.0;
				float hit_h = 0;
				float stepSize = h / _DisplacementStepCount;
				
				for (int it = 0; it < _DisplacementStepCount; ++it){ 
					h -= stepSize;
					NewCoords += dUV;
					float h_tex = tex2D(_DisplacementMap, NewCoords.xy).r;
					float is_first_hit = saturate((h_tex - h - prev_hits) * 4999999);
					hit_h += is_first_hit * h;
					prev_hits += is_first_hit;
				}
				
				NewCoords = i.tex + dUV * (1.0f-hit_h) * 10.0f - dUV;
				float2 Temp = NewCoords;
				h = hit_h+stepSize;
				float Start = h;
				dUV *= 0.2f;
				prev_hits = 0.0f;
				hit_h = 0.0f;
				
				float stepSizeref = stepSize / _DisplacementRefinement;
				
				for(int it = 0;it < _DisplacementRefinement ; ++it)
				{
					h -= stepSizeref;
					NewCoords += dUV;
					float CurrentHeight = tex2D(_DisplacementMap, NewCoords.xy).r;
					float first_hit = clamp((CurrentHeight - h - prev_hits) * 499999.0, 0.0f, 1.0f);
					hit_h += first_hit * h;
					prev_hits += first_hit;
				}
				NewCoords = Temp + dUV * (Start - hit_h) * 50.0f;
				
				fixed4 tex = tex2D(_MainTex, NewCoords.xy );
				fixed4 texN = tex2D(_BumpMap, NewCoords.xy );
				
				//unpackNormal function
				fixed3 localCoords = float3(2.0 * texN.ag - float2(1.0, 1.0),_NormalPower);
				
				//Normal transpose matrix
				half3x3 local2WorldTranspose = half3x3(
					i.tangentWorld,
					i.binormalWorld,
					i.normalWorld
				);
				
				//calculate normal direction
				fixed3 normalDirection = normalize( mul(localCoords, local2WorldTranspose));

				//lighting
				//dot product
				fixed nDotL = saturate(dot(normalDirection, i.lightDirection.xyz));
				
				fixed3 diffuseReflection = i.lightDirection.w * _LightColor0.xyz * nDotL;
				fixed3 specularReflection = diffuseReflection * _SpecColor.xyz * pow(saturate(dot(reflect( -i.lightDirection.xyz, normalDirection), i.viewDirection)), _Shininess);
				
				//Rim lighting
				fixed rim = 1 - nDotL;// Mask
				fixed3 rimLighting = nDotL * _LightColor0.xyz * _RimColor.xyz * pow(rim, _RimPower);
				
				fixed3 lightFinal = rimLighting + diffuseReflection + specularReflection + UNITY_LIGHTMODEL_AMBIENT.xyz;
				
				return fixed4(tex.xyz * lightFinal * _Color.xyz, 1.0);
			}
			
			ENDCG
		}
		Pass{
			Tags{"LightMode" = "ForwardADD"}
			blend One One
			CGPROGRAM
			#pragma target 4.0
			#pragma vertex vert
			#pragma fragment frag
			 
			
			//user defined variables
			uniform fixed4 _Color;
			uniform fixed4 _SpecColor;
			uniform fixed4 _RimColor;
			
			uniform half _Shininess;
			uniform half _RimPower;
			uniform fixed _NormalPower;
			uniform float _Attenuation;
			
			uniform sampler2D _MainTex;
			uniform half4 _MainTex_ST;
			uniform sampler2D _BumpMap;
			uniform half4 _BumpMap_ST;
			uniform sampler2D _DisplacementMap;
			uniform half4 _Displacement_ST;
			uniform fixed _DisplacementPower;
			uniform int _DisplacementRefinement;
			uniform int _DisplacementStepCount;
			
			
			//Unity defined variables
			uniform half4 _LightColor0;
	
			//base input structs
			struct vertexInput{
				half4 vertex : POSITION;
				half3 normal : NORMAL;
				half4 texcoord : TEXCOORD0;
				half4 tangent : TANGENT;
			};
			
			struct vertexOutput{
				half4 pos : SV_POSITION;
				half4 tex : TEXCOORD0;
				fixed4 lightDirection : TEXCOORD1;
				fixed3 viewDirection : TEXCOORD2;
				fixed3 normalWorld : TEXCOORD3;
				fixed3 tangentWorld : TEXCOORD4;
				fixed3 binormalWorld : TEXCOORD5;
			};
			
			//vertex function
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				o.normalWorld = normalize(mul(half4(v.normal.xyz, 0.0), _World2Object).xyz);
				o.tangentWorld = normalize(mul(_Object2World, v.tangent).xyz);
				o.binormalWorld = normalize(cross(o.normalWorld, o.tangentWorld) * v.tangent.w);
				
				half4 posWorld = mul(_Object2World, v.vertex);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.tex = v.texcoord;
				o.viewDirection = normalize(_WorldSpaceCameraPos.xyz - posWorld.xyz);
				
				half3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - posWorld.xyz;
				half distance = length(fragmentToLightSource);
				
				half attenuation = lerp(1.0, 1.0/distance, _WorldSpaceLightPos0.w);
				half3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w));
				o.lightDirection = half4 (lightDirection, attenuation);
				
				return o;
			}
			
			//fragment funtion 
			fixed4 frag(vertexOutput i) : COLOR{

				float2 NewCoords = i.tex;	
				float2 dUV = -i.viewDirection.xy * _DisplacementPower;
				float prev_hits = 0.0;
				float h = 1.0;
				float hit_h = 0;
				float stepSize = h / _DisplacementStepCount;
				
				for (int it = 0; it < _DisplacementStepCount; ++it){ 
					h -= stepSize;
					NewCoords += dUV;
					float h_tex = tex2D(_DisplacementMap, NewCoords.xy).r;
					float is_first_hit = saturate((h_tex - h - prev_hits) * 4999999);
					hit_h += is_first_hit * h;
					prev_hits += is_first_hit;
				}
				
				NewCoords = i.tex + dUV * (1.0f-hit_h) * 10.0f - dUV;
				float2 Temp = NewCoords;
				h = hit_h+0.1f;
				float Start = h;
				dUV *= 0.2f;
				prev_hits = 0.0f;
				hit_h = 0.0f;
				float stepSizeref = stepSize / _DisplacementRefinement;
				
				for(int it = 0;it < _DisplacementRefinement ; ++it)
				{
					h -= stepSizeref;
					NewCoords += dUV;
					float CurrentHeight = tex2D(_DisplacementMap, NewCoords.xy).r;
					float first_hit = clamp((CurrentHeight - h - prev_hits) * 499999.0, 0.0f, 1.0f);
					hit_h += first_hit * h;
					prev_hits += first_hit;
				}
				NewCoords = Temp + dUV * (Start - hit_h) * 50.0f;
				
				fixed4 texN = tex2D(_BumpMap, NewCoords.xy );
				
				//unpackNormal function
				fixed3 localCoords = float3(2.0 * texN.ag - float2(1.0, 1.0),_NormalPower);
				
				//Normal transpose matrix
				half3x3 local2WorldTranspose = half3x3(
					i.tangentWorld,
					i.binormalWorld,
					i.normalWorld
				);
				
				//calculate normal direction
				fixed3 normalDirection = normalize( mul(localCoords, local2WorldTranspose));

				//lighting
				//dot product
				fixed nDotL = saturate(dot(normalDirection, i.lightDirection.xyz));
				
				fixed3 diffuseReflection = i.lightDirection.w * _LightColor0.xyz * nDotL;
				fixed3 specularReflection = diffuseReflection * _SpecColor.xyz * pow(saturate(dot(reflect( -i.lightDirection.xyz, normalDirection), i.viewDirection)), _Shininess);
				
				//Rim lighting
				fixed rim = 1 - nDotL;// Mask
				fixed3 rimLighting = nDotL * _LightColor0.xyz * _RimColor.xyz * pow(rim, _RimPower);
				
				fixed3 lightFinal = rimLighting + diffuseReflection + specularReflection;
				
				return fixed4(lightFinal * _Color.xyz, 1.0);
			}
			
			ENDCG
		}
	}
	//Fallback "Diffuse"
}
