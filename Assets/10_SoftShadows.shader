﻿Shader "Custom/10 - SoftShadow" {
	Properties {
		_Color ("Color Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_MainTex ("Diffuse Texture", 2D) = "white" {}
		_IsPcfEnabled ("PcfEnabled", Float) = 1.0
		_Bias ("Bias", Range(0.001,0.002)) = 0.0015
		_LightRange ("Light Range", Float) = 10
		_ShadowingEnabled ("Enable Self-Shadowing", Range(0, 1)) = 1
		_LightEnabled ("Enable Light", Range(0, 1)) = 1
		_LinearAttenEnabled ("Enable Linear Attenuation", Range(0, 1)) = 0
		_QuadraticAttenEnabled ("Enable Quadratic Attenuation", Range(0, 1)) = 0
		_ConstantLinearQuadraticAttenEnabled ("Enable Constant Linear Quadratic Attenuation", Range(0, 1)) = 1
		
	}
	SubShader {
		
		// First Pass only consists out of a dummy shader because
		// Unity doesn't allow processing of point lights in 
		// the first pass if LightMode is set to ForwardBase.
		// This only works if LightMode is set to ForwardAdd, but
		// ForwardAdd doesn't work in the first pass. If no LightMode
		// is set, pointlights do work, but position changes of the 
		// light during are not propagated to the shader anymore.  
		
		Pass {
			Tags {"LightMode" = "ForwardBase"}
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0 
			
			#include "UnityCG.cginc"
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
			};
			
			//vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//fragment shader
			float4 frag(vertexOutput input) : COLOR {	
				return float4(0.0, 0.0, 0.0, 0.0);
			}
			ENDCG
		}
		
		//Second pass is set to ForwardAdd
		
		Pass {
			Tags {"LightMode" = "ForwardAdd"}
			Blend One Zero
			CGPROGRAM
			
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 5.0 
			
			#include "UnityCG.cginc"
			
			//user defined variables
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float4 _Color;
			uniform float _LightRange;
			uniform float _LightEnabled;
			uniform float _LinearAttenEnabled;
			uniform float _QuadraticAttenEnabled;
			uniform float _ConstantLinearQuadraticAttenEnabled;
			uniform sampler2D _DepthTexture;
			uniform float4x4 _DepthMatrix;
			uniform float _IsPcfEnabled;
			uniform float _Bias;
			
			//Unity defined variables
			uniform float4 _LightColor0;
			
			//base input structs
			struct vertexInput {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 tex : TEXCOORD0;
				float4 posWorld : TEXCOORD1;
				float4 posLight : TEXCOORD5;	//Pos as viewed from light source
			};
			
			//Vertex shader
			vertexOutput vert(vertexInput input) {
				vertexOutput output;
				
				//Calculate normalized normal in objectspace (because we have a directional light)
				output.posWorld = mul( _Object2World, input.vertex ); // kann raus
				output.tex = input.texcoord;
				output.posLight = mul( _Object2World, input.vertex );
				output.posLight = mul( _DepthMatrix, output.posLight );
				
				output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
				return output;
			}
			
			//Fragment shader
			float4 frag(vertexOutput input) : COLOR {
				float3 lightDirection;
				float atten = 0;
				float2 projectTexCoord;
				float depthValue = 0.0;
				float3 diffuseReflection = float3(0.0, 0.0, 0.0);
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - input.posWorld.xyz);
				
				float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz - input.posWorld.xyz;
				float r = length(fragmentToLightSource);
				atten = min(_LinearAttenEnabled, saturate(1-r/_LightRange));
				//atten = _LightRange/pow(r,2);
				atten += min(_QuadraticAttenEnabled, saturate(1-pow((r/_LightRange),2)));
				float sc = r;
				float sl = 0.7;
				float sq = 0.7;
				atten += min(_ConstantLinearQuadraticAttenEnabled, _LightRange/(sc + sl*r + sq * pow(r,2)));
				lightDirection = normalize(fragmentToLightSource);
				
				//Texture Maps
				float4 tex = tex2D(_MainTex, input.tex.xy * _MainTex_ST.xy + _MainTex_ST.zw);

    			projectTexCoord.x = input.posLight.x / input.posLight.w;
    			projectTexCoord.y = input.posLight.y / input.posLight.w;
				
				// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
    			if((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
    			{
    				// Calculate the depth of the light.
        			float lightDepthValue = input.posLight.z / input.posLight.w;
        			
        			// Subtract the bias from the lightDepthValue.
        			lightDepthValue = lightDepthValue - 0.00003;//_Bias;
        			
    				// Sample the shadow map depth value from the depth texture using the sampler at the projected texture coordinate location.
        			for (int i=-1; i<2; i++) {
        				for (int j=-1; j<2; j++) {					
        					float tmpDepthValue = tex2D(_DepthTexture, projectTexCoord.xy + float2(i*1.0f/1024,j*1.0f/1024)).r;// + _DepthTexture_ST.zw).r;	// sample depth texture with offset, to get neigbour pixels
        					depthValue += saturate((tmpDepthValue - lightDepthValue) * 999999); //multiply by high number to receive only either 0 or 1 and nothing in between	( depends on neighbour) results own lighting value between 0 and 1 dependent on the neighbour
        				}
        			}
        			depthValue /= 9; // calc average
        			
        			float depthValueHard = tex2D(_DepthTexture, projectTexCoord.xy).r;
        			depthValueHard = saturate((depthValueHard - lightDepthValue) * 999999); //multiply by high number to receive only either 0 or 1 and nothing in between
        			
        			// Determine wether hard- or softshadows are switched on
        			depthValue = _IsPcfEnabled * depthValue + (1.0-_IsPcfEnabled) * depthValueHard;
        			
					diffuseReflection = depthValue * atten * _LightColor0.rgb;		
				}
				
				float3 lightFinal = UNITY_LIGHTMODEL_AMBIENT.rgb + ceil(max(0.0, _LightEnabled)) * (diffuseReflection);
				
				return float4(tex.xyz * lightFinal * _Color.rgb, 1.0);
			}
			
			ENDCG
		}
	}
}
