﻿using UnityEngine;
using System.Collections;

public class SoftShadows : MonoBehaviour {
	Camera rtCam;
	RenderTexture depthTexture;
	Matrix4x4 lightMatrix;
	Matrix4x4 texMatrix;
	Vector4 offset;
	public GameObject displayPlane;
	public Light shadowLight;

	// Use this for initialization
	void Start () {
		rtCam = GetComponent<Camera> ();
		rtCam.transform.position = shadowLight.transform.position;
		//rtCam.transform.LookAt (new Vector3 (0.0f, 0.0f, 0.0f));
		rtCam.transform.LookAt ( new Vector3 (-2.533698f, 0.0f, 23.21529f)); 
		//rtCam.depthTextureMode = DepthTextureMode.Depth;
		//rtCam.targetTexture.format = RenderTextureFormat.Depth;
		//depthTexture = new RenderTexture((int)rtCam.pixelWidth, (int)rtCam.pixelHeight, 24, RenderTextureFormat.Depth);
		depthTexture = new RenderTexture(1024, 1024, 24, RenderTextureFormat.Depth); // textur format ist nicht rgb sondern black white
		int i = 0;
		//Only for quadratic texture sizes. For non quadratic you need different x and y offsets. 
		for (float j=-1.5f; j<=1.5f; j+=1.0f) {// kann evtl auch raus
			offset [i] = j * depthTexture.texelSize.x;
			i++;
		}

		depthTexture.wrapMode = TextureWrapMode.Clamp;// clamp = abschneiden 
		depthTexture.filterMode = FilterMode.Point; // bileanear kann man sich spaaren 
		rtCam.targetTexture = depthTexture;
		//displayRtMat.SetTexture ("_MainTex", depthTexture);
		displayPlane.GetComponent<Renderer>().material.SetTexture ("_MainTex", depthTexture);
		//depthTexture.SetGlobalShaderProperty ("_MainTex");
		texMatrix = Matrix4x4.identity;
		texMatrix[0,0] = 0.5f;
		texMatrix[1,1] = 0.5f;
		texMatrix[2,2] = 0.5f;
		texMatrix[0,3] = 0.5f;
		texMatrix[1,3] = 0.5f;
		texMatrix[2,3] = 0.5f;
		lightMatrix = texMatrix * rtCam.projectionMatrix * rtCam.worldToCameraMatrix;
		//lightMatrix = rtCam.worldToCameraMatrix * rtCam.projectionMatrix;
		//lightMatrix = texMatrix;
		Shader.SetGlobalVector ("_DepthTextureOffset", offset);// kann evtl auch raus
		Shader.SetGlobalMatrix("_DepthMatrix", lightMatrix);
		Shader.SetGlobalTexture ("_DepthTexture", depthTexture);
	}

	// Update is called once per frame
	void Update () {
		rtCam.transform.position = shadowLight.transform.position;
		//rtCam.transform.LookAt (new Vector3 (0.0f, 0.0f, 0.0f)); 
		rtCam.transform.LookAt ( new Vector3 (-2.533698f, 0.0f, 23.21529f)); 
		lightMatrix = texMatrix * rtCam.projectionMatrix * rtCam.worldToCameraMatrix;
		//lightMatrix = rtCam.worldToCameraMatrix * rtCam.projectionMatrix;
		//lightMatrix = texMatrix;
		Shader.SetGlobalMatrix("_DepthMatrix", lightMatrix);
		Shader.SetGlobalVector ("_LightPos", shadowLight.transform.position);// kann evtl auch raus
	}
}
