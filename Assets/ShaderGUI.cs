﻿using UnityEngine;
using System.Collections;

public class ShaderGUI : MonoBehaviour {
	public float normalPower = 1.0f;
	public Color diffColor = Color.white;

	public Color specColor = Color.white;
	public float shininess = 10.0f;

	public Color rimColor = Color.white;
	public float rimPower = 3.0f;

	public float displacementPower = 0.001f;
	public int stepCount = 1;
	public int refCount = 1;

	public float velocityX = 1.0f;
	public float velocityY = 0.0f;
	public float velocityZ = 0.0f;

	public bool softShadow = true;
	public float lightPos = -3.539557f;
	public float lifetime = 4.0f;
	public float size = 0.5f;

	Material normal;
	Material displacement;
	Material particle;
	Material softSjadow;
	GameObject pointLight;
	GameObject normalPlane;
	GameObject dissplacePlane;
	GameObject shadowPlane;
	MouseLook mouselook;
	bool mouseEnabled;
	
	// Use this for initialization
	void Start () {
		mouseEnabled = false;
		mouselook = this.gameObject.GetComponent<MouseLook>();
		mouselook.enabled = mouseEnabled;
		normalPlane = (GameObject)GameObject.FindGameObjectWithTag("Normal");
		dissplacePlane = (GameObject)GameObject.FindGameObjectWithTag("Displacement");
		shadowPlane = (GameObject)GameObject.FindGameObjectWithTag("SoftShadows");
		pointLight = GameObject.FindGameObjectWithTag("PointLight");

		//normal = Resources.Load<Material>("7_NormalMap") as Material;
		//displacement = Resources.Load<Material>("8_DisplacementMaps");
		//particle = Resources.Load<Material>("9_Particle");
		//softSjadow = Resources.Load<Material>("10_SoftShadows");

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.V)){
			if(!mouseEnabled){
				mouseEnabled = true;
			}else{
				mouseEnabled = false;
			}
		}
		mouselook.enabled = mouseEnabled;
	}

	void OnGUI () {
		// Make a background box
		GUI.Box(new Rect(10,10,200,90), "NormalMapping");
		GUI.Label (new Rect (20,30,100,50), "Power");
		normalPower = GUI.HorizontalSlider(new Rect(65, 35, 100, 30), normalPower, 1.0f, -1.0f);
		normalPlane.GetComponent<Renderer>().material.SetFloat("_NormalPower",normalPower);
		dissplacePlane.GetComponent<Renderer>().material.SetFloat("_NormalPower",normalPower);

		//-----------------------------------------------------------------------------------------------------
		GUI.Box(new Rect(10,110,200,50), "Specular");
		GUI.Label (new Rect (20,130,100,50), "shininess");
		shininess = GUI.HorizontalSlider(new Rect(85, 135, 100, 30), shininess, 10.0f, 0.0f);
		normalPlane.GetComponent<Renderer>().material.SetFloat("_Shininess",shininess);
		dissplacePlane.GetComponent<Renderer>().material.SetFloat("_Shininess",shininess);
		//-----------------------------------------------------------------------------------------------------
		GUI.Box(new Rect(10,170,200,50), "RimLighting");
		GUI.Label (new Rect (20,190,100,50), "Power");
		rimPower = GUI.HorizontalSlider(new Rect(65, 195, 100, 30), rimPower, 10.0f, 0.01f);
		normalPlane.GetComponent<Renderer>().material.SetFloat("_RimPower",rimPower);
		dissplacePlane.GetComponent<Renderer>().material.SetFloat("_RimPower",rimPower);
		//-----------------------------------------------------------------------------------------------------
		GUI.Box(new Rect(10,230,200,100), "DisplacementMapping");
		GUI.Label (new Rect (20,250,100,50), "Power");
		displacementPower = GUI.HorizontalSlider(new Rect(65, 255, 100, 30), displacementPower, 0.0f, 0.0015f);
		dissplacePlane.GetComponent<Renderer>().material.SetFloat("_DisplacementPower",displacementPower);

		GUI.Label (new Rect (20,280,100,50), "StepCount");
		stepCount = (int)GUI.HorizontalSlider(new Rect(85, 285, 100, 30), stepCount, 1, 10);
		dissplacePlane.GetComponent<Renderer>().material.SetFloat("_DisplacementStepCount",stepCount);

		GUI.Label (new Rect (20,310,100,50), "RefCount");
		refCount = (int)GUI.HorizontalSlider(new Rect(85, 315, 100, 30), refCount, 1, 10);
		dissplacePlane.GetComponent<Renderer>().material.SetFloat("_DisplacementRefinement",refCount);
		//-----------------------------------------------------------------------------------------------------
		GUI.Box(new Rect(10,340,200,170), "Particle");
		GUI.Label (new Rect (20,360,100,50), "VeloX");
		velocityX = GUI.HorizontalSlider(new Rect(85, 365, 100, 30), velocityX, 10.0f, 0.0f);
		Shader.SetGlobalFloat("_velocityX", velocityX);

		GUI.Label (new Rect (20,390,100,50), "VeloY");
		velocityY = GUI.HorizontalSlider(new Rect(85, 395, 100, 30), velocityY, 10.0f, 0.0f);
		Shader.SetGlobalFloat("_velocityY", velocityY);

		GUI.Label (new Rect (20,420,100,50), "VeloZ");
		velocityZ = GUI.HorizontalSlider(new Rect(85, 425, 100, 30), velocityZ, 10.0f, 0.0f);
		Shader.SetGlobalFloat("_velocityZ",velocityZ);

		GUI.Label (new Rect (20,450,100,50), "lifetime");
		lifetime = GUI.HorizontalSlider(new Rect(85, 455, 100, 30), lifetime, 0.0f, 10.0f);
		Shader.SetGlobalFloat("_LifeTime", lifetime);

		GUI.Label (new Rect (20,480,100,50), "size");
		size = GUI.HorizontalSlider(new Rect(85, 485, 100, 30), size, 0.0f, 10.0f);
		Shader.SetGlobalFloat("_Size", size);

		//-----------------------------------------------------------------------------------------------------
		GUI.Box(new Rect(10,520,200,70), "SoftShadow");
		softShadow = GUI.Toggle(new Rect(20, 540, 100, 30), softShadow, "Soft Shadows");
		if(softShadow){
			shadowPlane.GetComponent<Renderer>().material.SetFloat("_IsPcfEnabled",1.0f);
		}else{
			shadowPlane.GetComponent<Renderer>().material.SetFloat("_IsPcfEnabled",0.0f);
		}

		GUI.Label (new Rect (20,560,100,50), "Light Pos");
		lightPos = GUI.HorizontalSlider(new Rect(85, 565, 100, 30), lightPos, -10.0F, 5.0F);
		pointLight.transform.position = new Vector3(lightPos,2.652552f,29.6968f);


	}

}
